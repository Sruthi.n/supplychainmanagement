package com.SPC.IBL.flow;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.SPC.CommonFunctions.ApplicationCommonFunctions;
import com.SPC.IBL.CompanyDetails;
import com.SPC.Login.Login;
import com.SPC.base.ActionEngin;

public class Groups extends ActionEngin{
	
@Test
	
	public static void CreateNewGroups_flow() throws InterruptedException{
	
	Login.U_login();
	System.out.println("User1 Login");
	ApplicationCommonFunctions.CompaniesNav();
	
	WebElement CompanyName_Search = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CompanyName_Search"));
	WebElement CompanyType_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CompanyType_select"));
	WebElement Search_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Search_btn"));
	
	ActionEngin.sendData(CompanyName_Search, getExcelData().getSingleData("CompanyDetails", 6, 1));
	ActionEngin.selectVisibleText(CompanyType_select, "Anchor");
	Search_btn.click();
	Thread.sleep(2000);
	CompanyDetails.CreateNewGroup();
	Thread.sleep(2000);
}

}
