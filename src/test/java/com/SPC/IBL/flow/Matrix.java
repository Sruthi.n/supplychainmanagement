package com.SPC.IBL.flow;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.SPC.CommonFunctions.ApplicationCommonFunctions;
import com.SPC.IBL.CompanyDetails;
import com.SPC.Login.Login;
import com.SPC.base.ActionEngin;

public class Matrix extends ActionEngin{
	
@Test
	
	public static void CreateNewMatrix_flow() throws InterruptedException{
	
	Login.U_login();
	System.out.println("User1 Login");
	ApplicationCommonFunctions.CompaniesNav();
	
	WebElement CompanyName_Search = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CompanyName_Search"));
	WebElement CompanyType_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CompanyType_select"));
	WebElement Search_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Search_btn"));
	
	ActionEngin.sendData(CompanyName_Search, getExcelData().getSingleData("CompanyName_Search", 6, 1));
	ActionEngin.selectVisibleText(CompanyType_select, "Anchor");
	Search_btn.click();
	Thread.sleep(2000);
	CompanyDetails.CreateNewMatrixRule();
	
}

}
