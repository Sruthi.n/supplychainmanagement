package com.SPC.IBL.flow;

import org.testng.annotations.Test;

import com.SPC.CommonFunctions.ApplicationCommonFunctions;
import com.SPC.IBL.AddPrograms_DealerMapping;
import com.SPC.IBL.CompanyDetails;
import com.SPC.Login.Login;
import com.SPC.base.ActionEngin;


public class DealerFinancing extends ActionEngin {
	@Test
	
	public static void DealerMap_flow() throws InterruptedException{
		/*
		 * Update test data excel sheet Company details with
		 * CompanyName and Company's Unique Identification  
		 */
		Login.U_login();
		System.out.println("User1 Login");
		CompanyDetails.Add_CompanyDetails();
		System.out.println("2 Companies Added");
		Login.Logout();
		System.out.println("User1 Logout");
		Login.U2_login();
		System.out.println("User2 Login");
		
		ApplicationCommonFunctions.CompaniesNav();
		ApplicationCommonFunctions.CompanyName_Search(getExcelData().getSingleData("CompanyDetails", 6, 1));
		ApplicationCommonFunctions.Approval_action();
		System.out.println("Company1 Approved");
		
		ApplicationCommonFunctions.CompaniesNav();
		ApplicationCommonFunctions.CompanyName_Search(getExcelData().getSingleData("CompanyDetails", 6, 2));
		ApplicationCommonFunctions.Approval_action();
		System.out.println("Company 2 Approved");
		
		//Login.U2_login();
		AddPrograms_DealerMapping.Add_ProgramDetails_Dealer();
		Thread.sleep(5000);
		AddPrograms_DealerMapping.Add_DealerMapping();
		Thread.sleep(5000);
		System.out.println("Programs added and Dealer mapped");
		Login.Logout();
		System.out.println("User2 Logout");
		
		Login.U_login();
		System.out.println("User1 Login");
		ApplicationCommonFunctions.ProgramsNav();
		ApplicationCommonFunctions.ProgramName_Search(getExcelData().getSingleData("CompanyDetails", 6, 1));
		ApplicationCommonFunctions.Approval_action();
		System.out.println("Programm Approved");
		
		ApplicationCommonFunctions.ProgramsNav();
		ApplicationCommonFunctions.ProgramName_Search(getExcelData().getSingleData("CompanyDetails", 6, 1));
		ApplicationCommonFunctions.Approval_action();
		System.out.println("Programm Mapping Approved");
		Login.Logout();
		System.out.println("User1 Logout");
		Login.CloseWindow();
		System.out.println("Window closed");
		
	}

}
