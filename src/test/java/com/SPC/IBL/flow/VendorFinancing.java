package com.SPC.IBL.flow;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.SPC.CommonFunctions.ApplicationCommonFunctions;
import com.SPC.IBL.AddPrograms_VendorMapping;
import com.SPC.IBL.CompanyDetails;
import com.SPC.Login.Login;
import com.SPC.base.ActionEngin;

@Test

public class VendorFinancing extends ActionEngin{
	
	public static void VendorMap_flow() throws InterruptedException{
	
//		Login.U_login();
//		System.out.println("User1 Login");
//		CompanyDetails.Add_CompanyDetails();
//		System.out.println("2 Companies Added");
//		Login.Logout();
//		System.out.println("User1 Logout");
//		Login.U2_login();
//		System.out.println("User2 Login");
//		
//		ApplicationCommonFunctions.CompaniesNav();
//		ApplicationCommonFunctions.CompanyName_Search(getExcelData().getSingleData("CompanyDetails", 6, 1));
//		ApplicationCommonFunctions.Approval_action();
//		System.out.println("Company1 Approved");
//		
//		ApplicationCommonFunctions.CompaniesNav();
//		ApplicationCommonFunctions.CompanyName_Search(getExcelData().getSingleData("CompanyDetails", 6, 2));
//		ApplicationCommonFunctions.Approval_action();
//		System.out.println("Company 2 Approved");
		
		Login.U2_login();
		AddPrograms_VendorMapping.Add_ProgramDetails_Vendor();
		Thread.sleep(5000);
		AddPrograms_VendorMapping.Add_VendorMapping();
		Thread.sleep(2000);
		System.out.println("Programs added and vendor mapped");
		Login.Logout();
		System.out.println("User2 Logout");
		
		Login.U_login();
		System.out.println("User1 Login");
		ApplicationCommonFunctions.ProgramsNav();
		ApplicationCommonFunctions.ProgramName_Search(getExcelData().getSingleData("CompanyDetails", 6, 1));
		ApplicationCommonFunctions.Approval_action();
		System.out.println("Programm Approved");
		
		ApplicationCommonFunctions.ProgramName_Search(getExcelData().getSingleData("CompanyDetails", 6, 1));
		WebElement Manage_Dealers = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Manage_Dealers"));
		Manage_Dealers.click();
		ApplicationCommonFunctions.Approval_action();
		System.out.println("Programm Mapping Approved");
		Login.Logout();
		System.out.println("User1 Logout");
		Login.CloseWindow();
		System.out.println("Window closed");
	}
	

}
