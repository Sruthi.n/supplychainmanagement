package com.SPC.IBL.flow;

import org.testng.annotations.Test;

import com.SPC.CommonFunctions.ApplicationCommonFunctions;
import com.SPC.IBL.CompanyDetails;
import com.SPC.Login.Login;
import com.SPC.base.ActionEngin;

public class CreateNewUser extends ActionEngin{
	@Test
	
	public static void CreateNewUser_flow() throws InterruptedException{
		
		Login.U2_login();
		System.out.println("User1 Login");
		ApplicationCommonFunctions.CompaniesNav();
		ApplicationCommonFunctions.CompanyName_Search(getExcelData().getSingleData("CompanyDetails", 6, 1));
		CompanyDetails.CreateNewUser();
		System.out.println("1 CreateNewUser Added");
		Thread.sleep(2000);
		Login.Logout();
		System.out.println("User1 Logout");
		
		Login.U_login();
		System.out.println("User2 Login");
		ApplicationCommonFunctions.CompaniesNav();
		ApplicationCommonFunctions.CompanyName_Search(getExcelData().getSingleData("CompanyDetails", 6, 1));
		ApplicationCommonFunctions.ViewUser_action();
		ApplicationCommonFunctions.CompanyName_Search(getExcelData().getSingleData("CompanyDetails", 39, 1));
		ApplicationCommonFunctions.Approval_action();
		System.out.println("NewUser Approved");
		Thread.sleep(2000);
		Login.Logout();
		System.out.println("User1 Logout");
		Login.CloseWindow();
		System.out.println("Window closed");
		
	}

}
