package com.SPC.IBL;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.SPC.Login.Login;
import com.SPC.base.ActionEngin;

public class InvoiceDetailPage extends ActionEngin {
	
	@Test
	public static void CreateNewInvoice() throws InterruptedException{
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		Login.Login_vendorUser();
		System.out.println("Vendor user Login");
		WebElement Invoice_tab = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Invoice_tab"));
		Invoice_tab.click();
		Thread.sleep(2000);
		WebElement CreateInvoice = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CreateInvoice"));
		CreateInvoice.click();
		Thread.sleep(2000);
		
		WebElement Anchor = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Anchor"));
		WebElement LoanAccount_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("LoanAccount_select"));
		WebElement Type_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Type_select"));
		WebElement InvoiceNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("InvoiceNo"));
		WebElement AssociatedPO = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AssociatedPO"));
		WebElement InvoiceDate = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("InvoiceDate"));
		WebElement DueDate = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("DueDate"));
		WebElement Currency = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Currency"));
		WebElement Remarks = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Remarks"));
		WebElement Item = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Item"));
		WebElement Quantity = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Quantity"));
		WebElement Unit = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Unit"));
		WebElement UnitPrice = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("UnitPrice"));
		WebElement Total = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Total"));
		WebElement Description = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Description"));
		WebElement Percentage_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Percentage_select"));
		WebElement Discount = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Discount"));
		WebElement DiscountAmt = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("DiscountAmt"));
		WebElement AddItem = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AddItem"));
		WebElement Submit = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit"));
		Anchor.click();
		WebElement AnchorInputBox = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AnchorInputBox"));
		AnchorInputBox.click();
		ActionEngin.sendData(AnchorInputBox, getExcelData().getSingleData("UserVendor", 1, 1));
		AnchorInputBox.sendKeys(Keys.ENTER);
		ActionEngin.selectVisibleText(LoanAccount_select, getExcelData().getSingleData("UserVendor", 2, 1));
		ActionEngin.selectVisibleText(Type_select, getExcelData().getSingleData("UserVendor", 3, 1));
		ActionEngin.sendData(InvoiceNo, getExcelData().getSingleData("UserVendor", 4, 1));
		InvoiceDate.click();
		Thread.sleep(2000);
		ActionEngin.getDate(InvoiceDate, getExcelData().getSingleData("UserVendor", 6, 1));
		DueDate.click();
		Thread.sleep(2000);
		ActionEngin.getDate(DueDate, getExcelData().getSingleData("UserVendor", 7, 1));
		ActionEngin.sendData(Remarks, getExcelData().getSingleData("UserVendor", 9, 1));
		Thread.sleep(2000);
		js.executeScript("window.scrollBy(400,400)");
		ActionEngin.sendData(Item, getExcelData().getSingleData("UserVendor", 10, 1));
		ActionEngin.sendData(Quantity, getExcelData().getSingleData("UserVendor", 11, 1));
		ActionEngin.sendData(Unit, getExcelData().getSingleData("UserVendor", 12, 1));
		Thread.sleep(2000);
		UnitPrice.click();
		Thread.sleep(2000);

		ActionEngin.sendData(UnitPrice, getExcelData().getSingleData("UserVendor", 18, 1));
		System.out.println("UnitPrice entered");
		Total.click();
		Thread.sleep(1000);
		//ActionEngin.sendData(Total, getExcelData().getSingleData("UserVendor", 13, 1));
		ActionEngin.sendData(Description, getExcelData().getSingleData("UserVendor", 14, 1));
		ActionEngin.selectVisibleText(Percentage_select, getExcelData().getSingleData("UserVendor", 15, 1));
		ActionEngin.sendData(Discount, getExcelData().getSingleData("UserVendor", 16, 1));
		ActionEngin.sendData(DiscountAmt, getExcelData().getSingleData("UserVendor", 17, 1));
		System.out.println("Invoice form entered");
		js.executeScript("window.scrollBy(400,400)");
		Submit.click();
		Thread.sleep(3000);
		
	}

}
