package com.SPC.IBL;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.SPC.CommonFunctions.ApplicationCommonFunctions;
import com.SPC.Login.Login;
import com.SPC.base.ActionEngin;

public class CompanyDetails extends ActionEngin {
	@Test
	public static void Add_CompanyDetails() throws InterruptedException{
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		//Login.U_login();
		Thread.sleep(2000);
		
		
		int i ;
		for (i=1;i<3;i++){
			
			ApplicationCommonFunctions.CompaniesNav();
			WebElement AddNewCompany = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AddNewCompany"));
			AddNewCompany.click();
			Thread.sleep(2000);

		/****************************************Company Details*********************************************/

		WebElement Company_Name = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Company_Name"));
		WebElement Company_UID = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Company_UID"));
		WebElement CIF = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CIF"));
		WebElement Branch_code = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Branch_code"));
		WebElement Business_ID = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Business_ID"));
		WebElement Organization_Type_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Organization_Type_select"));
		WebElement Business_Segment_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Business_Segment_select"));
		WebElement Industry = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Industry"));
		WebElement PAN = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("PAN"));
		WebElement GST = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("GST"));
		WebElement LEI_Code = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("LEI_Code"));
		WebElement UCIC = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("UCIC"));
		WebElement CUST_ANCODE = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CUST_ANCODE"));
		WebElement PSL_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("PSL_select"));
		WebElement Company_Logo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Company_Logo"));

		ActionEngin.sendData(Company_Name, getExcelData().getSingleData("CompanyDetails", 6, i));
		ActionEngin.sendData(Company_UID, getExcelData().getSingleData("CompanyDetails", 7, i));
		CIF.clear();
		ActionEngin.sendData(CIF, getExcelData().getSingleData("CompanyDetails", 8, i));
		Branch_code.click();
		WebElement Branch_code_input = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Branch_code_input"));
		ActionEngin.sendData(Branch_code_input, getExcelData().getSingleData("CompanyDetails", 9, i));
		Thread.sleep(1000);
		//Branch_code_input.sendKeys(Keys.RETURN);
		Branch_code_input.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		ActionEngin.sendData(Business_ID, getExcelData().getSingleData("CompanyDetails", 10, i));
		ActionEngin.selectVisibleText(Organization_Type_select, getExcelData().getSingleData("CompanyDetails", 11, i));
		ActionEngin.selectVisibleText(Business_Segment_select, getExcelData().getSingleData("CompanyDetails", 12, i));
		Industry.click();
		WebElement Industry_input = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Industry_input"));
		ActionEngin.sendData(Industry_input, getExcelData().getSingleData("CompanyDetails", 13, i));
		Thread.sleep(1000);
		Industry_input.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		ActionEngin.sendData(PAN, getExcelData().getSingleData("CompanyDetails", 14, i));
		ActionEngin.sendData(GST, getExcelData().getSingleData("CompanyDetails", 15, i));
		ActionEngin.sendData(LEI_Code, getExcelData().getSingleData("CompanyDetails", 16, i));
		ActionEngin.sendData(UCIC, getExcelData().getSingleData("CompanyDetails", 17, i));
		ActionEngin.sendData(CUST_ANCODE, getExcelData().getSingleData("CompanyDetails", 18, i));
		ActionEngin.sendData(PSL_select, getExcelData().getSingleData("CompanyDetails", 19, i));
		js.executeScript("window.scrollBy(400,400)");
		/****************************************Company Address Details******************************************/

		WebElement Country = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Country"));
		//WebElement State = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("State"));
		//WebElement City = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("City"));
		WebElement PostalCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("PostalCode"));
		WebElement Address = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Address"));

		Country.click();
		WebElement Country_Input = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Country_Input"));
		ActionEngin.sendData(Country_Input, getExcelData().getSingleData("CompanyDetails", 25, i));
		Country_Input.sendKeys(Keys.ARROW_DOWN);
		Country_Input.sendKeys(Keys.ENTER);
		Thread.sleep(4000);

		Boolean staleElement = true; 

		while(staleElement){

			try{

				WebElement State = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("State"));
				State.click();
				WebElement State_Input = ActionEngin.identifyElement("xpath",getObjRep().getPropertyValue("State_Input"));
				ActionEngin.sendData(State_Input, getExcelData().getSingleData("CompanyDetails", 26, i));
				State_Input.sendKeys(Keys.ENTER);
				Thread.sleep(2000);

				WebElement City = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("City"));
				City.click();
				WebElement City_Input = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("City_Input"));
				ActionEngin.sendData(City_Input,getExcelData().getSingleData("CompanyDetails", 27, i));
				City_Input.sendKeys(Keys.ENTER); 
				Thread.sleep(2000);

				staleElement = false;

			} catch(StaleElementReferenceException e){
				staleElement = true;
			}
		}

		ActionEngin.sendData(PostalCode, getExcelData().getSingleData("CompanyDetails", 28, i));
		ActionEngin.sendData(Address, getExcelData().getSingleData("CompanyDetails", 29, i));

		/****************************************Relationship Manager Details******************************************/
		js.executeScript("window.scrollBy(400,400)");
		WebElement RelationshipMgr_Name = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("RelationshipMgr_Name"));
		RelationshipMgr_Name.click();

		WebElement RelationshipMGR_Name_input = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("RelationshipMGR_Name_input"));
		ActionEngin.sendData(RelationshipMGR_Name_input, getExcelData().getSingleData("CompanyDetails", 34, i));
		Thread.sleep(1000);

		RelationshipMGR_Name_input.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		System.out.println("Add New Company Details Submit");

		WebElement Submit = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit"));
		WebElement Cancel = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Cancel"));

		Submit.click();
		Thread.sleep(5000);
		
	}
	/*	WebElement MyProfile = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("MyProfile"));
		WebElement logout = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("logout"));
		
		MyProfile.click();
		Thread.sleep(1000);
		logout.click();
		*/
	}
	
	public static void CreateNewUser() throws InterruptedException{

		ApplicationCommonFunctions.ViewUser_action();
		int i ;
		for (i=1;i<3;i++){
			ApplicationCommonFunctions.CreateNewUser_action();
			WebElement Name = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Name"));
			WebElement Email = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Email"));
			WebElement Mobile = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Mobile"));
			WebElement Roles_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Roles_select"));
			WebElement ReceiveNotifcation_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ReceiveNotifcation_select"));
			WebElement Status_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Status_select"));
			WebElement Submit_CreateNewUser = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit_CreateNewUser"));

			ActionEngin.sendData(Name, getExcelData().getSingleData("CompanyDetails", 39, i));
			ActionEngin.sendData(Email, getExcelData().getSingleData("CompanyDetails", 40, i));
			ActionEngin.sendData(Mobile, getExcelData().getSingleData("CompanyDetails", 41, i));
			ActionEngin.selectVisibleText(Roles_select, getExcelData().getSingleData("CompanyDetails", 42, i));
			ActionEngin.selectVisibleText(ReceiveNotifcation_select, getExcelData().getSingleData("CompanyDetails", 43, i));
			ActionEngin.selectVisibleText(Status_select, getExcelData().getSingleData("CompanyDetails", 44, i));

			WebElement VendorFinancingGroup = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("VendorFinancingGroup"));
			WebElement DealerFinancingGroup = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("DealerFinancingGroup"));

			VendorFinancingGroup.click();
			Thread.sleep(2000);
			//ActionEngin.selectVisibleText(VendorFinancingGroup, getExcelData().getSingleData("CompanyDetails", 45, 1));
			VendorFinancingGroup.sendKeys(Keys.ARROW_DOWN);
			VendorFinancingGroup.sendKeys(Keys.ENTER);
			//ActionEngin.selectVisibleText(DealerFinancingGroup, getExcelData().getSingleData("CompanyDetails", 46, 1));
			Submit_CreateNewUser.click();
			Thread.sleep(2000);
		}

	}
	
	public static void CreateNewGroup() throws InterruptedException{
		
		ApplicationCommonFunctions.CreateNewGroup_action();
		WebElement Group_Name = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Group_Name"));
		WebElement Group_Level = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Group_Level"));
		//WebElement Group_Product = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Group_Product"));
		//WebElement Product_Vendor = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Product_Vendor"));
		WebElement Product_Dealer = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Product_Dealer"));
		WebElement Group_StatusSelect = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Group_StatusSelect"));
		WebElement Group_Submit = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Group_Submit"));
		
		ActionEngin.sendData(Group_Name, getExcelData().getSingleData("CompanyDetails", 49, 1));
		ActionEngin.sendData(Group_Level, getExcelData().getSingleData("CompanyDetails", 50, 1));
		Thread.sleep(2000);
		ActionEngin.sendData(Product_Dealer, getExcelData().getSingleData("CompanyDetails", 51, 1));
		Thread.sleep(1000);
		Product_Dealer.sendKeys(Keys.RETURN);
		ActionEngin.selectVisibleText(Group_StatusSelect, getExcelData().getSingleData("CompanyDetails", 52, 1));
		Group_Submit.click();
		
	}
	
	public static void CreateNewMatrixRule() throws InterruptedException{
		
		ApplicationCommonFunctions.AuthorizationMatrix_action();
		
		WebElement Matrix_Financing = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Matrix_Financing_select"));
		WebElement Matrix_Name = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Matrix_Name"));
		WebElement Matrix_Status = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Matrix_Status"));
		WebElement Matrix_MinAmount = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Matrix_MinAmount"));
		WebElement Matrix_MaxAmount = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Matrix_MaxAmount"));
		WebElement Matrix_GrpName = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Matrix_GrpName"));
		WebElement Matrix_MinApproval = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Matrix_MinApproval"));
		WebElement Matrix_AddMore = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Matrix_AddMore"));
		
		ActionEngin.selectVisibleText(Matrix_Financing, getExcelData().getSingleData("CompanyDetails", 55, 1));
		ActionEngin.sendData(Matrix_Name, getExcelData().getSingleData("CompanyDetails", 56, 1));
		ActionEngin.selectVisibleText(Matrix_Status, getExcelData().getSingleData("CompanyDetails", 57, 1));
		ActionEngin.sendData(Matrix_MinAmount, getExcelData().getSingleData("CompanyDetails", 58, 1));
		ActionEngin.sendData(Matrix_MaxAmount, getExcelData().getSingleData("CompanyDetails", 59, 1));
		
		Matrix_AddMore.click();
		
	}

}
