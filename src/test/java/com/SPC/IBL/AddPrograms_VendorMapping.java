package com.SPC.IBL;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.SPC.CommonFunctions.ApplicationCommonFunctions;
import com.SPC.Login.Login;
import com.SPC.base.ActionEngin;

public class AddPrograms_VendorMapping extends ActionEngin{
	@Test
	public static void Add_ProgramDetails_Vendor() throws InterruptedException{
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		//Login.U_login();
		Thread.sleep(2000);

		ApplicationCommonFunctions.ProgramsNav();
		WebElement AddNewProgram = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AddNewProgram"));
		AddNewProgram.click();
		Thread.sleep(2000);

		WebElement ProductType_select = ActionEngin.identifyElement("id", "product_type");
		WebElement Anchor_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Anchor_select"));
		WebElement ProgramCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ProgramCode"));
		WebElement ProductCode_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ProductCode_select"));
		WebElement ProgramApprovedDate = ActionEngin.identifyElement("name", "approved_date");
		WebElement LimitExpiryDate = ActionEngin.identifyElement("name", "limit_expiry_date");
		WebElement TotalProgramLimit = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("TotalProgramLimit"));
		WebElement MaximumLimitperAccount = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("MaximumLimitperAccount"));
		WebElement CollectionAccount = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CollectionAccount"));
		WebElement Eligibility = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Eligibility"));
		WebElement RequestAutoFinance_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("RequestAutoFinance_select"));
		WebElement StaleInvoicePeriod = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("StaleInvoicePeriod"));
		WebElement MinimumFinancingDays = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("MinimumFinancingDays"));
		WebElement MaximumFinancingDays = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("MaximumFinancingDays"));

		ActionEngin.selectVisibleText(ProductType_select, getExcelData().getSingleData("ProgramDetails_Vendor", 2, 1));
		ActionEngin.selectVisibleText(Anchor_select, getExcelData().getSingleData("ProgramDetails_Vendor", 3, 1));
		ActionEngin.sendData(ProgramCode, getExcelData().getSingleData("ProgramDetails_Vendor", 5, 1));
		ActionEngin.selectVisibleText(ProductCode_select, getExcelData().getSingleData("ProgramDetails_Vendor", 6, 1));
		ActionEngin.getDate(ProgramApprovedDate, getExcelData().getSingleData("ProgramDetails_Vendor", 7, 1));
		ActionEngin.sendData(TotalProgramLimit, getExcelData().getSingleData("ProgramDetails_Vendor", 8, 1));
		Thread.sleep(2000);
		ActionEngin.getDate(LimitExpiryDate, getExcelData().getSingleData("ProgramDetails_Vendor", 9, 1));
		ActionEngin.sendData(MaximumLimitperAccount, getExcelData().getSingleData("ProgramDetails_Vendor", 10, 1));
		ActionEngin.sendData(CollectionAccount, getExcelData().getSingleData("ProgramDetails_Vendor", 11, 1));
		ActionEngin.sendData(Eligibility, getExcelData().getSingleData("ProgramDetails_Vendor", 12, 1));
		ActionEngin.selectVisibleText(RequestAutoFinance_select, getExcelData().getSingleData("ProgramDetails_Vendor", 13, 1));
		Thread.sleep(2000);
		ActionEngin.sendData(StaleInvoicePeriod, getExcelData().getSingleData("ProgramDetails_Vendor", 14, 1));
		ActionEngin.sendData(MinimumFinancingDays, getExcelData().getSingleData("ProgramDetails_Vendor", 15, 1));
		ActionEngin.sendData(MaximumFinancingDays, getExcelData().getSingleData("ProgramDetails_Vendor", 16, 1));
		js.executeScript("window.scrollBy(400,400)");

		WebElement Segment_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Segment_select"));
		WebElement AutoDebitAnchorforFinancedInvoices_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AutoDebitAnchorforFinancedInvoices_select"));
		WebElement AutoDebitAnchorforNonFinancedInvoices_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AutoDebitAnchorforNonFinancedInvoices_select"));
		WebElement AllowAnchortoChangeDueDate_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AllowAnchortoChangeDueDate_select"));
		WebElement DefaultPaymentTerms = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("DefaultPaymentTerms"));
		WebElement RepaymentAppropriation_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("RepaymentAppropriation_select"));
		WebElement InvoiceAttachmentMandatory = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("InvoiceAttachmentMandatory"));
		WebElement Recourse = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Recourse"));
		WebElement CompanyBoardResolutionAttachment = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CompanyBoardResolutionAttachment"));
		WebElement Status = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Status"));

		ActionEngin.selectVisibleText(Segment_select, getExcelData().getSingleData("ProgramDetails_Vendor", 17, 1));
		ActionEngin.selectVisibleText(AutoDebitAnchorforFinancedInvoices_select, getExcelData().getSingleData("ProgramDetails_Vendor", 18, 1));
		ActionEngin.sendData(DefaultPaymentTerms, getExcelData().getSingleData("ProgramDetails_Vendor", 23, 1));
		ActionEngin.selectVisibleText(RepaymentAppropriation_select, getExcelData().getSingleData("ProgramDetails_Vendor", 24, 1));
		ActionEngin.selectVisibleText(InvoiceAttachmentMandatory, getExcelData().getSingleData("ProgramDetails_Vendor", 25, 1));
		ActionEngin.selectVisibleText(Recourse, getExcelData().getSingleData("ProgramDetails_Vendor", 26, 1));
		ActionEngin.selectVisibleText(Status, getExcelData().getSingleData("ProgramDetails_Vendor", 28, 1));
		Thread.sleep(3000);
		js.executeScript("window.scrollBy(400,400)");

		WebElement BenchmarkTitle_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("BenchmarkTitle_select"));
		WebElement Spread = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Spread"));
		WebElement AnchorInterestBearing = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AnchorInterestBearing"));
		WebElement InterestType_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("InterestType_select"));
		WebElement GracePeriod = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("GracePeriod"));
		WebElement PenalInterestOnPrincipal = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("PenalInterestOnPrincipal"));
		WebElement FeeName = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("FeeName"));
		WebElement Type_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Type_select"));
		WebElement Value = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Value"));
		WebElement AnchorBearing = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AnchorBearing"));
		WebElement Taxes = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Taxes"));

		ActionEngin.selectVisibleText(BenchmarkTitle_select, getExcelData().getSingleData("ProgramDetails_Vendor", 31, 1));
		ActionEngin.sendData(Spread, getExcelData().getSingleData("ProgramDetails_Vendor", 34, 1));
		ActionEngin.sendData(AnchorInterestBearing, getExcelData().getSingleData("ProgramDetails_Vendor", 35, 1));
		ActionEngin.selectVisibleText(InterestType_select, getExcelData().getSingleData("ProgramDetails_Vendor", 37, 1));
		ActionEngin.sendData(GracePeriod, getExcelData().getSingleData("ProgramDetails_Vendor", 38, 1));
		ActionEngin.sendData(PenalInterestOnPrincipal, getExcelData().getSingleData("ProgramDetails_Vendor", 39, 1));
		Thread.sleep(3000);
		js.executeScript("window.scrollBy(200,200)");

		ActionEngin.sendData(FeeName, getExcelData().getSingleData("ProgramDetails_Vendor", 44, 1));
		ActionEngin.selectVisibleText(Type_select, getExcelData().getSingleData("ProgramDetails_Vendor", 45, 1));
		ActionEngin.sendData(Value, getExcelData().getSingleData("ProgramDetails_Vendor", 46, 1));
		ActionEngin.sendData(AnchorBearing, getExcelData().getSingleData("ProgramDetails_Vendor", 47, 1));
		ActionEngin.sendData(Taxes, getExcelData().getSingleData("ProgramDetails_Vendor", 49, 1));
		Thread.sleep(4000);
		Taxes.sendKeys(Keys.ENTER);
		js.executeScript("window.scrollBy(400,400)");

		WebElement AnchorEmailID = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AnchorEmailID"));
		WebElement AnchorMobileNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AnchorMobileNo"));
		WebElement NameAsPerBank = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("NameAsPerBank"));
		WebElement AccountNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AccountNo"));
		WebElement BankName = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("BankName_select"));
		WebElement Branch = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Branch"));
		WebElement BranchIFSCCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("BranchIFSCCode"));
		WebElement AccountType = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AccountType"));

		ActionEngin.sendData(AnchorEmailID, getExcelData().getSingleData("ProgramDetails_Vendor", 52, 1));
		ActionEngin.sendData(AnchorMobileNo, getExcelData().getSingleData("ProgramDetails_Vendor", 53, 1));
		ActionEngin.sendData(NameAsPerBank, getExcelData().getSingleData("ProgramDetails_Vendor", 59, 1));
		ActionEngin.sendData(AccountNo, getExcelData().getSingleData("ProgramDetails_Vendor", 60, 1));
		ActionEngin.selectVisibleText(BankName, getExcelData().getSingleData("ProgramDetails_Vendor", 61, 1));
		ActionEngin.sendData(Branch, getExcelData().getSingleData("ProgramDetails_Vendor", 62, 1));
		ActionEngin.sendData(BranchIFSCCode, getExcelData().getSingleData("ProgramDetails_Vendor", 63, 1));
		ActionEngin.sendData(AccountType, getExcelData().getSingleData("ProgramDetails_Vendor", 64, 1));
		Thread.sleep(4000);
		js.executeScript("window.scrollBy(200,200)");
		System.out.println("Add Program Page Filled");

		//WebElement Submit_Programs_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit_Programs_btn"));
		//WebElement Cancel_Programs_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Cancel_Programs_btn"));
		WebElement Submit_Mapping = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit_Mapping"));
		
		ActionEngin.waitForElement(Submit_Mapping, 50);
		Submit_Mapping.click();
		System.out.println("Submit Vendor Mapping clicked");
		Thread.sleep(5000);

		//Add_VendorMapping();

	}	

	//@Test (dependsOnMethods = "Add_ProgramDetails_Vendor")
	@Test
	
	public static void Add_VendorMapping() throws InterruptedException{

		JavascriptExecutor js = (JavascriptExecutor) getDriver();

		WebElement Vendor_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Vendor_select"));
		WebElement LoanODAccountNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("LoanODAccountNo"));
		WebElement SanctionedLimit = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("SanctionedLimit"));
		WebElement LimitApprovedDate = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("LimitApprovedDate"));
		WebElement LimitExpiryDate = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("LimitExpiryDate"));
		WebElement LimitReviewDate = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("LimitReviewDate"));
		WebElement DrawingPower = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("DrawingPower"));
		WebElement RequestAutoFinance_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("RequestAutoFinance_select"));
		WebElement AutoApproveFinance_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AutoApproveFinance_select"));
		WebElement Eligibility = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Eligibility"));
		WebElement SchemeCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("SchemeCode"));
		WebElement VendorCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("VendorCode"));
		WebElement Status_id = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Status_id"));
		
		ActionEngin.waitForElement(Vendor_select, 50);
		ActionEngin.selectVisibleText(Vendor_select, getExcelData().getSingleData("VendorMapping", 4, 1));
		ActionEngin.sendData(LoanODAccountNo, getExcelData().getSingleData("VendorMapping", 5, 1));
		ActionEngin.sendData(SanctionedLimit, getExcelData().getSingleData("VendorMapping", 6, 1));
		ActionEngin.getDate(LimitApprovedDate, getExcelData().getSingleData("VendorMapping", 7, 1));
		ActionEngin.getDate(LimitExpiryDate, getExcelData().getSingleData("VendorMapping", 8, 1));
		ActionEngin.getDate(LimitReviewDate, getExcelData().getSingleData("VendorMapping", 9, 1));
		ActionEngin.sendData(DrawingPower, getExcelData().getSingleData("VendorMapping", 10, 1));
		ActionEngin.selectVisibleText(RequestAutoFinance_select, getExcelData().getSingleData("VendorMapping", 11, 1));
		ActionEngin.selectVisibleText(AutoApproveFinance_select, getExcelData().getSingleData("VendorMapping", 12, 1));
		ActionEngin.sendData(Eligibility, getExcelData().getSingleData("VendorMapping", 13, 1));
		ActionEngin.sendData(SchemeCode, getExcelData().getSingleData("VendorMapping", 14, 1));
		js.executeScript("window.scrollBy(200,200)");
		ActionEngin.sendData(VendorCode, getExcelData().getSingleData("VendorMapping", 16, 1));
		ActionEngin.selectVisibleText(Status_id, getExcelData().getSingleData("VendorMapping", 17, 1));
		js.executeScript("window.scrollBy(400,400)");
		Thread.sleep(2000);

		WebElement VendorEmail = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("VendorEmail"));
		WebElement MobileNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("MobileNo"));

		js.executeScript("window.scrollBy(400,400)");
		ActionEngin.sendData(VendorEmail, getExcelData().getSingleData("VendorMapping", 38, 1));
		ActionEngin.sendData(MobileNo, getExcelData().getSingleData("VendorMapping", 39, 1));
		js.executeScript("window.scrollBy(400,400)");

		WebElement NameAsPerBank = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("NameAsPerBank"));
		WebElement AccountNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AccountNo"));
		WebElement BankName = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("BankName_select"));
		WebElement Branch = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Branch"));
		WebElement BranchIFSCCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("BranchIFSCCode"));
		WebElement AccountType = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AccountType"));

		ActionEngin.sendData(NameAsPerBank, getExcelData().getSingleData("VendorMapping", 43, 1));
		ActionEngin.sendData(AccountNo, getExcelData().getSingleData("VendorMapping", 44, 1));
		ActionEngin.selectVisibleText(BankName, getExcelData().getSingleData("VendorMapping", 45, 1));
		ActionEngin.sendData(Branch, getExcelData().getSingleData("VendorMapping", 46, 1));
		ActionEngin.sendData(BranchIFSCCode, getExcelData().getSingleData("VendorMapping", 47, 1));
		ActionEngin.sendData(AccountType, getExcelData().getSingleData("VendorMapping", 48, 1));
		Thread.sleep(4000);

		WebElement SubmitVendor_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("SubmitVendor_btn"));
		//WebElement Cancel_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Cancel_btn"));
		js.executeScript("window.scrollBy(400,400)");
		SubmitVendor_btn.click();
		Thread.sleep(4000);
		System.out.println("Submit clicked at Vendor Mapping");
		WebElement popup_ok = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("popup_ok"));
		popup_ok.click();
		System.out.println("POP_UP OK clicked at Vendor Mapping");

	}

}
