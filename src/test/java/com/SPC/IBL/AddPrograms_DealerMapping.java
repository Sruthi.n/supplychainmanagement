package com.SPC.IBL;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.SPC.CommonFunctions.ApplicationCommonFunctions;
import com.SPC.Login.Login;
import com.SPC.base.ActionEngin;

public class AddPrograms_DealerMapping extends ActionEngin{

	@Test

	public static void Add_ProgramDetails_Dealer() throws InterruptedException{

		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		//Login.U_login();
		Thread.sleep(2000);

		ApplicationCommonFunctions.ProgramsNav();
		WebElement AddNewProgram = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AddNewProgram"));
		AddNewProgram.click();
		Thread.sleep(2000);

		WebElement ProductType_select = ActionEngin.identifyElement("id", "product_type");
		ActionEngin.selectVisibleText(ProductType_select, getExcelData().getSingleData("ProgramDetails_Vendor", 67, 1));

		WebElement Anchor_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Anchor_select"));
		WebElement ProgramCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ProgramCode"));
		WebElement Segment_dealer = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Segment_dealer"));
		WebElement ProgramApprovedDate = ActionEngin.identifyElement("name", "approved_date");
		WebElement TotalProgramLimit = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("TotalProgramLimit"));
		WebElement LimitExpiryDate = ActionEngin.identifyElement("name", "limit_expiry_date");
		WebElement MaximumLimitperAccount = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("MaximumLimitperAccount"));
		WebElement RequestAutoFinance_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("RequestAutoFinance_select"));
		WebElement StaleInvoicePeriod = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("StaleInvoicePeriod"));
		WebElement StopSupplyDays = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("StopSupplyDays"));
		WebElement FLDG = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("FLDG"));
		WebElement DefaultPaymentTerms = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("DefaultPaymentTerms"));
		WebElement InvoiceAttachmentMandatory = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("InvoiceAttachmentMandatory"));
		WebElement CompanyBoardResolutionAttachment = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CompanyBoardResolutionAttachment"));
		WebElement Status = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Status"));

		ActionEngin.selectVisibleText(Anchor_select, getExcelData().getSingleData("ProgramDetails_Vendor", 3, 1));
		ActionEngin.sendData(ProgramCode, getExcelData().getSingleData("ProgramDetails_Vendor", 5, 1));
		ActionEngin.selectVisibleText(Segment_dealer, getExcelData().getSingleData("ProgramDetails_Vendor", 68, 1));
		ActionEngin.getDate(ProgramApprovedDate, getExcelData().getSingleData("ProgramDetails_Vendor", 7, 1));
		ActionEngin.sendData(TotalProgramLimit, getExcelData().getSingleData("ProgramDetails_Vendor", 8, 1));
		Thread.sleep(2000);
		ActionEngin.getDate(LimitExpiryDate, getExcelData().getSingleData("ProgramDetails_Vendor", 9, 1));
		ActionEngin.sendData(MaximumLimitperAccount, getExcelData().getSingleData("ProgramDetails_Vendor", 10, 1));
		ActionEngin.selectVisibleText(RequestAutoFinance_select, getExcelData().getSingleData("ProgramDetails_Vendor", 13, 1));
		Thread.sleep(2000);
		ActionEngin.sendData(StaleInvoicePeriod, getExcelData().getSingleData("ProgramDetails_Vendor", 14, 1));
		ActionEngin.sendData(StopSupplyDays, getExcelData().getSingleData("ProgramDetails_Vendor", 69, 1));
		ActionEngin.sendData(FLDG, getExcelData().getSingleData("ProgramDetails_Vendor", 70, 1));
		ActionEngin.sendData(DefaultPaymentTerms, getExcelData().getSingleData("ProgramDetails_Vendor", 23, 1));
		js.executeScript("window.scrollBy(400,400)");
		ActionEngin.selectVisibleText(InvoiceAttachmentMandatory, getExcelData().getSingleData("ProgramDetails_Vendor", 25, 1));
		ActionEngin.selectVisibleText(Status, getExcelData().getSingleData("ProgramDetails_Vendor", 28, 1));
		Thread.sleep(3000);


		WebElement BenchmarkTitleDealer_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("BenchmarkTitleDealer_select"));
		WebElement ToDay = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ToDay"));
		WebElement Interest = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Interest"));
		WebElement GracePeriod = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("GracePeriod"));
		WebElement PenalInterestOnPrincipal = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("PenalInterestOnPrincipal"));
		WebElement FeeName = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("FeeName"));
		WebElement Type_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Type_select"));
		WebElement Value = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Value"));
		WebElement DealorBearing = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("DealorBearing"));
		WebElement Taxes = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Taxes"));

		ActionEngin.selectVisibleText(BenchmarkTitleDealer_select, getExcelData().getSingleData("ProgramDetails_Vendor", 74, 1));
		ActionEngin.sendData(ToDay, getExcelData().getSingleData("ProgramDetails_Vendor", 72, 1));
		Thread.sleep(3000);
		ActionEngin.sendData(Interest, getExcelData().getSingleData("ProgramDetails_Vendor", 73, 1));
		ActionEngin.sendData(GracePeriod, getExcelData().getSingleData("ProgramDetails_Vendor", 38, 1));
		ActionEngin.sendData(PenalInterestOnPrincipal, getExcelData().getSingleData("ProgramDetails_Vendor", 39, 1));
		Thread.sleep(3000);
		ActionEngin.sendData(FeeName, getExcelData().getSingleData("ProgramDetails_Vendor", 44, 1));
		ActionEngin.selectVisibleText(Type_select, getExcelData().getSingleData("ProgramDetails_Vendor", 45, 1));
		ActionEngin.sendData(Value, getExcelData().getSingleData("ProgramDetails_Vendor", 46, 1));
		//ActionEngin.sendData(AnchorBearing, getExcelData().getSingleData("ProgramDetails_Vendor", 47, 1));
		ActionEngin.sendData(Taxes, getExcelData().getSingleData("ProgramDetails_Vendor", 49, 1));
		Thread.sleep(4000);
		Taxes.sendKeys(Keys.ENTER);
		js.executeScript("window.scrollBy(400,400)");
		System.out.println("Interest Over");
		WebElement AnchorEmailID = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AnchorEmailID"));
		WebElement AnchorMobileNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AnchorMobileNo"));
		WebElement NameAsPerBank = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("NameAsPerBank"));
		WebElement AccountNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AccountNo"));
		WebElement BankName = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("BankName_select"));
		WebElement Branch = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Branch"));
		WebElement BranchIFSCCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("BranchIFSCCode"));
		WebElement AccountType = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AccountType"));

		ActionEngin.sendData(AnchorEmailID, getExcelData().getSingleData("ProgramDetails_Vendor", 52, 1));
		ActionEngin.sendData(AnchorMobileNo, getExcelData().getSingleData("ProgramDetails_Vendor", 53, 1));
		System.out.println("Email Over");
		ActionEngin.sendData(NameAsPerBank, getExcelData().getSingleData("ProgramDetails_Vendor", 59, 1));
		ActionEngin.sendData(AccountNo, getExcelData().getSingleData("ProgramDetails_Vendor", 60, 1));
		Thread.sleep(4000);
		ActionEngin.selectVisibleText(BankName, getExcelData().getSingleData("ProgramDetails_Vendor", 61, 1));
		ActionEngin.sendData(Branch, getExcelData().getSingleData("ProgramDetails_Vendor", 62, 1));
		ActionEngin.sendData(BranchIFSCCode, getExcelData().getSingleData("ProgramDetails_Vendor", 63, 1));
		ActionEngin.sendData(AccountType, getExcelData().getSingleData("ProgramDetails_Vendor", 64, 1));
		Thread.sleep(4000);
		js.executeScript("window.scrollBy(200,200)");

		//WebElement Submit_Programs_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit_Programs_btn"));
		//WebElement Cancel_Programs_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Cancel_Programs_btn"));
		WebElement Submit_DealerMapping = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit_DealerMapping"));

		Submit_DealerMapping.click();
		System.out.println("Submit Dealer Mapping clicked");
		Thread.sleep(5000);
		
	}
	
	@Test (dependsOnMethods = "Add_ProgramDetails_Dealer")

	public static void Add_DealerMapping() throws InterruptedException{

		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		Thread.sleep(5000);
		WebElement Dealer_select = ActionEngin.identifyElement("name", "distributor");
		ActionEngin.waitForElement(Dealer_select, 10);
		WebElement LoanODAccountNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("LoanODAccountNo"));
		WebElement SanctionedLimit = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("SanctionedLimit"));
		WebElement LimitApprovedDate = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("LimitApprovedDate"));
		WebElement OD_ExpiryDate = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("OD_ExpiryDate"));
		WebElement LimitReviewDate = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("LimitReviewDate"));
		WebElement DrawingPower = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("DrawingPower"));
		WebElement RequestAutoFinance_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("RequestAutoFinance_select"));
		WebElement AutoApproveFinance_select = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AutoApproveFinance_select"));
		WebElement Eligibility = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Eligibility"));
		WebElement SchemeCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("SchemeCode"));
		WebElement DealerCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("DealerCode"));
		WebElement Status_id = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Status_id"));

		ActionEngin.selectVisibleText(Dealer_select, getExcelData().getSingleData("DealerMapping", 4, 1));
		ActionEngin.sendData(LoanODAccountNo, getExcelData().getSingleData("DealerMapping", 5, 1));
		ActionEngin.sendData(SanctionedLimit, getExcelData().getSingleData("DealerMapping", 6, 1));
		ActionEngin.getDate(LimitApprovedDate, getExcelData().getSingleData("DealerMapping", 7, 1));
		ActionEngin.getDate(OD_ExpiryDate, getExcelData().getSingleData("DealerMapping", 8, 1));
		ActionEngin.getDate(LimitReviewDate, getExcelData().getSingleData("DealerMapping", 9, 1));
		ActionEngin.sendData(DrawingPower, getExcelData().getSingleData("DealerMapping", 10, 1));
		ActionEngin.selectVisibleText(RequestAutoFinance_select, getExcelData().getSingleData("DealerMapping", 11, 1));
		ActionEngin.selectVisibleText(AutoApproveFinance_select, getExcelData().getSingleData("DealerMapping", 12, 1));
		ActionEngin.sendData(Eligibility, getExcelData().getSingleData("DealerMapping", 13, 1));
		ActionEngin.sendData(SchemeCode, getExcelData().getSingleData("DealerMapping", 14, 1));
		js.executeScript("window.scrollBy(200,200)");
		ActionEngin.sendData(DealerCode, getExcelData().getSingleData("DealerMapping", 16, 1));
		ActionEngin.selectVisibleText(Status_id, getExcelData().getSingleData("DealerMapping", 17, 1));
		js.executeScript("window.scrollBy(400,400)");
		Thread.sleep(2000);
		js.executeScript("window.scrollBy(200,200)");
		WebElement VendorEmail = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("VendorEmail"));
		WebElement MobileNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("MobileNo"));

		ActionEngin.sendData(VendorEmail, getExcelData().getSingleData("DealerMapping", 38, 1));
		ActionEngin.sendData(MobileNo, getExcelData().getSingleData("DealerMapping", 39, 1));
		js.executeScript("window.scrollBy(400,400)");

		WebElement NameAsPerBank = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("NameAsPerBank"));
		WebElement AccountNo = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AccountNo"));
		WebElement BankName = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("BankName_select"));
		WebElement Branch = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Branch"));
		WebElement BranchIFSCCode = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("BranchIFSCCode"));
		WebElement AccountType = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("AccountType"));

		ActionEngin.sendData(NameAsPerBank, getExcelData().getSingleData("DealerMapping", 43, 1));
		ActionEngin.sendData(AccountNo, getExcelData().getSingleData("DealerMapping", 44, 1));
		ActionEngin.selectVisibleText(BankName, getExcelData().getSingleData("DealerMapping", 45, 1));
		ActionEngin.sendData(Branch, getExcelData().getSingleData("DealerMapping", 46, 1));
		ActionEngin.sendData(BranchIFSCCode, getExcelData().getSingleData("DealerMapping", 47, 1));
		ActionEngin.sendData(AccountType, getExcelData().getSingleData("DealerMapping", 48, 1));
		Thread.sleep(4000);
		System.out.println("Vendor Mapping Page Filled");

		WebElement SubmitDealer_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("SubmitDealer_btn"));
		//WebElement Cancel_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Cancel_btn"));
		js.executeScript("window.scrollBy(400,400)");
		SubmitDealer_btn.click();
		Thread.sleep(4000);
		System.out.println("Submit clicked at Dealer Mapping"); 
		WebElement popup_ok = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("popup_ok"));
		popup_ok.click();
		System.out.println("POP_UP OK clicked at Dealer Mapping");

	}





}
