package com.SPC.Login;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.SPC.base.ActionEngin;

public class Login extends ActionEngin{
	@Test

	public static void U_login() throws InterruptedException {
		System.out.println("Launching IBL instance");
		ActionEngin.enterURL(getExcelData().getSingleData("UserDetails", 0, 1), "driver");

		try {
			WebElement UNAME = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("username"));
			WebElement PWD = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("password"));
			WebElement LOGIN = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("login"));


			ActionEngin.waitForElement(UNAME, 30);
			ActionEngin.sendData(UNAME, getExcelData().getSingleData("UserDetails", 2, 0));
			Thread.sleep(2000);
			ActionEngin.sendData(PWD, getExcelData().getSingleData("UserDetails", 2, 1));
			Thread.sleep(2000);
			//System.out.println("Using userdetails--" + UNAME.getText() + getObjRep().getPropertyValue("password"));
			LOGIN.click();
			Thread.sleep(5000);
			WebElement OTP = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("otp"));
			WebElement Submit_otp = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit_otp"));
			ActionEngin.sendData(OTP, getExcelData().getSingleData("UserDetails", 2, 2));
			Submit_otp.click();
			Thread.sleep(5000);
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			System.out.println("Caught Stale element and Continued");
			Thread.sleep(5000);
			WebElement UNAME = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("username"));
			WebElement PWD = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("password"));
			WebElement LOGIN = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("login"));


			ActionEngin.waitForElement(UNAME, 30);
			ActionEngin.sendData(UNAME, getExcelData().getSingleData("UserDetails", 2, 0));
			Thread.sleep(2000);
			ActionEngin.sendData(PWD, getExcelData().getSingleData("UserDetails", 2, 1));
			Thread.sleep(2000);
			//System.out.println("Using userdetails--" + UNAME.getText() + getObjRep().getPropertyValue("password"));
			LOGIN.click();
			Thread.sleep(5000);
			WebElement OTP = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("otp"));
			WebElement Submit_otp = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit_otp"));
			ActionEngin.sendData(OTP, getExcelData().getSingleData("UserDetails", 2, 2));
			Submit_otp.click();
		}

		System.out.println("Sign in successfull");
		Thread.sleep(2000);

	}

	public static void U2_login() throws InterruptedException {

		System.out.println("Launching IBL instance");
		ActionEngin.enterURL(getExcelData().getSingleData("UserDetails", 0, 1), "driver");

		try {
			WebElement UNAME = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("username"));
			WebElement PWD = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("password"));
			WebElement LOGIN = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("login"));


			ActionEngin.waitForElement(UNAME, 20);
			ActionEngin.sendData(UNAME, getExcelData().getSingleData("UserDetails", 3, 0));
			Thread.sleep(2000);
			ActionEngin.sendData(PWD, getExcelData().getSingleData("UserDetails", 3, 1));
			Thread.sleep(2000);
			//System.out.println("Using userdetails--" + UNAME.getText() + getObjRep().getPropertyValue("password"));
			LOGIN.click();
			Thread.sleep(2000);
			WebElement OTP = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("otp"));
			WebElement Submit_otp = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit_otp"));
			ActionEngin.sendData(OTP, getExcelData().getSingleData("UserDetails", 3, 2));
			Submit_otp.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			System.out.println("Caught Stale element and Continued");
			Thread.sleep(2000);
			WebElement UNAME = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("username"));
			WebElement PWD = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("password"));
			WebElement LOGIN = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("login"));


			ActionEngin.waitForElement(UNAME, 20);
			ActionEngin.sendData(UNAME, getExcelData().getSingleData("UserDetails", 3, 0));
			Thread.sleep(2000);
			ActionEngin.sendData(PWD, getExcelData().getSingleData("UserDetails", 3, 1));
			Thread.sleep(2000);
			//System.out.println("Using userdetails--" + UNAME.getText() + getObjRep().getPropertyValue("password"));
			LOGIN.click();
			Thread.sleep(2000);
			WebElement OTP = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("otp"));
			WebElement Submit_otp = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit_otp"));
			ActionEngin.sendData(OTP, getExcelData().getSingleData("UserDetails", 3, 2));
			Submit_otp.click();
		}

		System.out.println("Sign in successfull");
		Thread.sleep(2000);

	}
	public static void Login_vendorUser() throws InterruptedException {
		System.out.println("Launching IBL instance");
		ActionEngin.enterURL(getExcelData().getSingleData("UserDetails", 0, 1), "driver");
		WebElement UNAME = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("username"));
		WebElement PWD = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("password"));
		WebElement LOGIN = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("login"));


		ActionEngin.waitForElement(UNAME, 20);
		ActionEngin.sendData(UNAME, getExcelData().getSingleData("UserDetails", 4, 0));
		Thread.sleep(2000);
		ActionEngin.sendData(PWD, getExcelData().getSingleData("UserDetails", 4, 1));
		Thread.sleep(2000);
		//System.out.println("Using userdetails--" + UNAME.getText() + getObjRep().getPropertyValue("password"));
		LOGIN.click();
		Thread.sleep(2000);
		WebElement OTP = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("otp"));
		WebElement Submit_otp = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Submit_otp"));
		ActionEngin.sendData(OTP, getExcelData().getSingleData("UserDetails", 4, 2));
		Submit_otp.click();
	}

	public static void Logout() throws InterruptedException {

		WebElement MyProfile = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("MyProfile"));
		ActionEngin.waitForElement(MyProfile, 50);
		MyProfile.click();
		Thread.sleep(1000);
		WebElement logout = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("logout"));
		ActionEngin.waitForElement(logout, 50);
		logout.click();
		Thread.sleep(5000);

	}

	public static void CloseWindow() throws InterruptedException {
		getDriver().close();
		Thread.sleep(2000);
	}


}
