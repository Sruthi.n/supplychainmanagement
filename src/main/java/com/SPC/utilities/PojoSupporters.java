package com.SPC.utilities;

import java.io.IOException;

public class PojoSupporters{
	
	private static PropertiesUtilities poConfiData,objRep;
	private static ExcelReader excelReaderPath;
	private static TextUtilities txtData;
	
	
	public static PropertiesUtilities getConfigData() throws IOException {
		 poConfiData = new PropertiesUtilities(FilePaths.confPath);
	return 	poConfiData;
	}
	public static PropertiesUtilities getObjRepo() throws IOException {
		 objRep = new PropertiesUtilities(FilePaths.orPath);
		 
	return 	objRep;
	}
	
	public static TextUtilities getTxtData() throws IOException{
		txtData = new TextUtilities(FilePaths.txtPath);
		return txtData;
		
	}
	
	public static ExcelReader getExcelData() throws IOException {
		 excelReaderPath = new ExcelReader(FilePaths.excelPath);
	return 	excelReaderPath;
	}

	
}
