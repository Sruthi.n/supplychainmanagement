package com.SPC.utilities;

import com.SPC.base.BaseEngin;

public interface FilePaths {
	
	String excelPath = System.getProperty("user.dir")+"\\TestData\\SupplyChainManagement_IBL.xls";
	String confPath  = System.getProperty("user.dir")+"\\Application.properties";
	String orPath    = System.getProperty("user.dir")+"\\src\\test\\java\\com\\auto\\objectrepository\\OR.properties";
	String txtPath   = System.getProperty("user.dir")+"\\TxtDataFile\\d.txt";
	String csvPath   = System.getProperty("user.dir")+"";
	String reportPath= System.getProperty("user.dir")+"\\Reports\\Results.html";
	String imagePath = BaseEngin.getCurDir()+"\\ScreenShots\\"+BaseEngin.getTcName()+System.currentTimeMillis()+".jpeg";
	//String imagePath ="D:\\viffinProjectBDD\\VeefinHybrid\\ScreenShots\\gmailTest.jpeg";
	
	
}
