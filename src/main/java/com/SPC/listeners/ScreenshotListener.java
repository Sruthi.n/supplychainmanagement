
package com.SPC.listeners;
import java.io.IOException;

import org.testng.ITestListener; 
import org.testng.ITestResult; 
import org.testng.TestListenerAdapter; 
import org.testng.internal.annotations.ListenersAnnotation;


import com.SPC.base.BaseEngin;
import com.relevantcodes.extentreports.LogStatus;
import com.SPC.utilities.*;
public class ScreenshotListener implements ITestListener{

	@Override public void onTestFailure(ITestResult result) {

		System.out.println(" TC is FAIL :"+result.getName());
		BaseEngin.getExtentTest().log(LogStatus.FAIL,
				"The test case is: "+BaseEngin.getTcName());
		BaseEngin.getExtentTest().log(LogStatus.FAIL, result.getThrowable()); 
		try {
			ScreenShotUtility.screenShot(); 
		} catch (IOException e) { 
			// TODO Auto-generated catch block 
			e.printStackTrace(); 
		} }

	@Override public void onTestSkipped(ITestResult result) {

		System.out.println(" TC is SKIPPED :"+result.getName());
		BaseEngin.getExtentTest().log(LogStatus.SKIP,
				"The test case is::"+result.getName());
		BaseEngin.getExtentTest().log(LogStatus.SKIP, result.getThrowable());
		try {
			ScreenShotUtility.screenShot(); 
		} catch (IOException e) { 
			// TODOAuto-generated catch block 
			e.printStackTrace(); } }

	@Override public void onTestStart(ITestResult result) {
		System.out.println("CURRENT TC EXECUTION IS : " +result.getName());
		BaseEngin.getExtentTest().log(LogStatus.INFO,
				"The test case onTestStart::"+result.getName()); }

	@Override public void onTestSuccess(ITestResult result) {

		System.out.println("TC IS PASSED : " +result.getName());
		BaseEngin.getExtentTest().log(LogStatus.INFO,
				"The test case onTestSuccess::"+result.getName()); 
		try {
			ScreenShotUtility.screenShot(); 
		} 
		catch (IOException e) { 
			// TODOAuto-generated catch block 
			e.printStackTrace(); 
		} }
}




