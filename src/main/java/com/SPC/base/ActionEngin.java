package com.SPC.base;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

abstract public class ActionEngin extends BaseEngin{

	private static WebElement webelement;
	private static List<WebElement> webelements;


	public static String enterURL(String url, String how) throws InterruptedException {
		if(how.equalsIgnoreCase("driver")) {
			System.out.println("Using URL--" + url);
			getDriver().get(url);
			Thread.sleep(5000);
		}else if(how.equalsIgnoreCase("navigate")) {
			getDriver().navigate().to(url);
			Thread.sleep(5000);
		}
		return how;
	}	


	public static WebElement identifyElement(String locMech,String locValue) {

		switch (locMech) {
		case "id":
			webelement= getDriver().findElement(By.id(locValue));
			checkElementVisibility(webelement);
			break;
		case "name":
			webelement= getDriver().findElement(By.name(locValue));
			checkElementVisibility(webelement);
			break;
		case "classname":
			webelement= getDriver().findElement(By.className(locValue));
			checkElementVisibility(webelement);
			break;
		case "css":
			webelement= getDriver().findElement(By.cssSelector(locValue));
			checkElementVisibility(webelement);
			break;
		case "xpath":
			webelement= getDriver().findElement(By.xpath(locValue));
			checkElementVisibility(webelement);
			break;
		case "linktext":
			webelement= getDriver().findElement(By.linkText(locValue));
			checkElementVisibility(webelement);
			break;
		case "partiallinktext":
			webelement= getDriver().findElement(By.partialLinkText(locValue));
			checkElementVisibility(webelement);
			break;
		case "tagname":
			webelement= getDriver().findElement(By.tagName(locValue));
			checkElementVisibility(webelement);
			break;

		default:
			System.out.println("LOCATOR IS NOT FOUND IN THE WEBPAGE");
			break;
		}
		return webelement;
	}

	public static List<WebElement> identifyElements(String locMech,String locValue) {

		switch (locMech) {
		case "id":
			webelements= getDriver().findElements(By.id(locValue));
			checkElementVisibility(webelements);
			break;
		case "name":
			webelements= getDriver().findElements(By.name(locValue));
			checkElementVisibility(webelements);
			break;
		case "classname":
			webelements= getDriver().findElements(By.className(locValue));
			checkElementVisibility(webelements);
			break;
		case "css":
			webelements= getDriver().findElements(By.cssSelector(locValue));
			checkElementVisibility(webelements);
			break;
		case "xpath":
			webelements= getDriver().findElements(By.xpath(locValue));
			checkElementVisibility(webelements);
			break;
		case "linktext":
			webelements= getDriver().findElements(By.linkText(locValue));
			checkElementVisibility(webelements);
			break;
		case "partiallinktext":
			webelements= getDriver().findElements(By.partialLinkText(locValue));
			checkElementVisibility(webelements);
			break;
		case "tagname":
			webelements= getDriver().findElements(By.tagName(locValue));
			checkElementVisibility(webelements);
			break;

		default:
			System.out.println("LOCATOR IS NOT FOUND IN THE WEBPAGE");
			break;
		}
		return webelements;
	}


	public static void enterData(String locMech,String locValue,String testdata) {
		webelement = identifyElement(locMech, locValue);
		webelement.sendKeys(testdata);
	}

	public static void sendData(WebElement webelement,String testdata) {
		webelement.sendKeys(testdata);
	}

	public static String getData(WebElement webelement) {
		return webelement.getText();
	}
	
	public static void getDate(WebElement webelement, String getDate) throws InterruptedException {
		webelement.click();
		WebElement YR = ActionEngin.identifyElement("classname", "ui-datepicker-year");


		System.out.println("Issue date is " + getDate);
		String date= getDate.substring(0, 11);
		System.out.println("Issue date is " + date);
		String splitter[] = date.split("-");
		String year = splitter[2];
		String month = splitter[1];
		String day = splitter[0]; 
		System.out.println(year);
		System.out.println(month);
		System.out.println(day);

		YR.click();
		ActionEngin.selectVisibleText(YR, year);
		System.out.println(year +"-year clicked");
		Thread.sleep(3000);
		Boolean staleElement = true; 

		while(staleElement){
			try {

				WebElement Mon = ActionEngin.identifyElement("classname", "ui-datepicker-month");
				Mon.click();
				ActionEngin.selectVisibleText(Mon, month);
				System.out.println(month +"-month clicked");
				Thread.sleep(3000);
				staleElement = false;
			}
			catch(StaleElementReferenceException e){
				staleElement = true;
			}
		}
		List<WebElement> rows,cols;
		//getting rows in the calendar body
		rows = ActionEngin.identifyElements("tagname", "tr");
		for (WebElement r:rows)
		{
			//System.out.println(r.getText());
			//getting the columns in rows
			cols= ActionEngin.identifyElements("tagname", "td");

			for (WebElement c:cols)
			{
				if(c.getText().equals(day))
				{
					System.out.println(c.getText());
					c.click();
					System.out.println("Date Clicked is ");
					Thread.sleep(2000);
					return;
				}
			}
		}

	}


	public static void clickElement(String locMech,String locValue,String clickType) {
		Actions actions = new  Actions(getDriver());

		webelement = identifyElement(locMech, locValue);

		if(clickType.equalsIgnoreCase("Webelement")) {
			webelement.click();

		}else if(clickType.equalsIgnoreCase("single")) {
			actions.click(webelement).perform();

		}else if(clickType.equalsIgnoreCase("double")) {
			actions.doubleClick(webelement).build().perform();

		}else if(clickType.equalsIgnoreCase("keyboard")) {
			actions.sendKeys(webelement,Keys.ENTER).build().perform();
		}
	}

	public static void mouseHoverActions(String locMech,String locValue ) {
		Actions actions = new  Actions(getDriver());
		webelement = identifyElement(locMech, locValue);
		actions.moveToElement(webelement).perform();	
	}

	public static void mouseHoverActions(WebElement element ) {
		Actions actions = new  Actions(getDriver());
		actions.moveToElement(element).perform();;	

		//actions.d
	}

	public static void clickElementSingle(String locMech,String locValue) {
		webelement = identifyElement(locMech, locValue);
		webelement.click();
	}

	public static void closeCurrentBrowser() {
		getDriver().close();
	}
	public static void closeAllBrowser() {
		getDriver().quit();
	}


	public static boolean checkElementVisibility(WebElement webelement) {
		Boolean status = (webelement.isDisplayed()&& webelement.isEnabled())?true:false;
		return status;
	}

	private static Boolean checkElementVisibility(List<WebElement>  webelements) {
		Boolean status = true;
		for(WebElement webelement : webelements) {
			status = (webelement.isDisplayed()&& webelement.isEnabled())?true:false;
		}
		return status;
	}

	public static void checkErrorMessage(By by, String errorMessage) {

		if(errorMessage.isEmpty()){
			WebElement errorElement = waitForElement(by, 1);

			if(errorElement==null){
				System.out.println("passed: error message is not populated.");
			}else {
				System.out.println("failed: error message is populated.");
			}
		}else {		
			//nagative case:

			WebElement errorElement = waitForElement(by, 1);			
			if(errorElement==null) {
				System.out.println("failed : is not populated");
			}else {
				String actualMessage = errorElement.getText();
				if(actualMessage.trim().equals(errorMessage)) {
					System.out.println("passed:error message is populated .");
				}else {
					System.out.println("failed: error message is populated.");
					Assert.assertEquals(actualMessage, errorMessage);
				}
			}
		}
	}		
	public static WebElement waitForElement(WebElement element, int timeTOWait) {

		WebDriverWait wait = new WebDriverWait(getDriver(), timeTOWait);
		wait.pollingEvery(Duration.ofMillis(200));
		wait.until(ExpectedConditions.visibilityOf(element));

		return element;
	}

	//=======================================================================================================================

	public static void getSystemHost(){

		try {		
			InetAddress ia = Inet4Address.getLocalHost();
			System.out.println(ia.getHostName());
			System.out.println(ia.getHostAddress());
			System.out.println(System.getProperty("os.name"));
			System.out.println(System.getProperty("os.arch"));

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}

	public static boolean checkInURL(String expURL) {

		boolean isValFound = false;
		try{
			String curURL = getDriver().getCurrentUrl();
			isValFound = curURL.toLowerCase().contains(expURL.toLowerCase());

		} catch (WebDriverException wde){
			System.out.println("Error while verifying the url. Exception message captured is :  " + wde.getMessage());
			Assert.assertTrue(false, "Unable to verify the url as webdriver exception found.");
		}
		return isValFound;
	}

	public static WebElement waitForElement(By by,int timeToWait){
		WebElement element = null;
		try{
			WebDriverWait wait = new WebDriverWait(getDriver(), timeToWait);
			wait.pollingEvery(Duration.ofMillis(200));
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));

		}catch(Exception e ){
			System.out.println("Element :"+by.toString()+" is not found even after waiting for" +timeToWait+ "secands:"); 
		}
		return element;
	}

	protected static WebElement searchElement(By by) {
		return getDriver().findElement(by);
	}

	protected void scrollDownToViewElement(By by) {

		((JavascriptExecutor)getDriver()).executeScript("arguments[0].scrollIntoView(false);",searchElement(by));
	}

	//---------------------------------------------------------------------------------

	protected  static void scrollToBottom(){
		((JavascriptExecutor)getDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public static void scrollToTop(){
		((JavascriptExecutor)getDriver()).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
	}

	public static void scrollElementintoView(By by,int timeToWait){         
		waitForElement(by, timeToWait);
		WebElement we=searchElement(by);
		scrollElementintoView(we);
	}

	public static void scrollElementintoView(WebElement we){         
		((JavascriptExecutor)getDriver()).executeScript("arguments[0].scrollIntoView(false);", we);//made this true to scroll elements which are below
	}
	//gets Date & Time in ddMMMhhmm format e.g. 16Mar1849
	protected String getCurrentDateTime(){
		DateFormat dateFormat = new SimpleDateFormat("ddMMMHHmm");
		return dateFormat.format(new Date());
	}

	public static By clickElementJavaScript(By by) {
		WebElement element = getDriver().findElement(by);	
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("arguments[0].click();", element);			    	
		return by;	
	}
	public static WebElement clickElementJavaScript(WebElement wb) {	
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("arguments[0].click();", wb);			    	
		return wb;	
	}
	protected void clickButtonAndWait(By by,int timeToWait) throws InterruptedException{
		waitForElement(by, timeToWait);
		clickButtonAndWait(searchElement(by));
	}

	protected void clickButtonAndWait(WebElement we) throws InterruptedException{        
		waitForElement(we, 1);
		we.click();	      
	}

	protected void scrollintoViewandClickButton(By by,int timeToWait){			

		WebElement we=searchElement(by);
		((JavascriptExecutor)getDriver()).executeScript("arguments[0].scrollIntoView(false);", we);
		waitForElement(by, timeToWait);
		we.click();
	}

	public static boolean switchToFrame(String frameTitle) {

		try {
			getDriver().switchTo().frame(frameTitle);
			return true;
		}catch(NoSuchElementException nsf ) {
			System.out.println("unable to switch to frame as  in the application.");
			return false;
		}        	
	}
	public static boolean checkElementvisibility(WebElement element, int maxtime){
		boolean isVisible;
		try{
			WebDriverWait wait = new WebDriverWait(getDriver(),maxtime);
			wait.until(ExpectedConditions.visibilityOf(element));
			wait.pollingEvery(Duration.ofMillis(200));
			isVisible = true;

		}catch(Exception e ){
			isVisible = false;
		}
		return isVisible;
	}

	public static boolean checkElementEnable(WebElement element, int maxtime){
		boolean isEnable;
		try{
			WebDriverWait wait = new WebDriverWait(getDriver(), maxtime);
			wait.pollingEvery(Duration.ofMillis(200));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			isEnable = true;
		}catch(Exception e){
			isEnable = false; 
		}
		return isEnable;
	}

	public static void selectCheckBox(By by ){
		WebElement element = waitForElement(by, 2);
		if(element != null){
			if(checkElementvisibility(element, 2)){
				if(checkElementEnable(element, 2)){
					if(!element.isSelected()){
						element.click();
					}
				}else{
					System.out.println("unable to select the checkbox :"+by.toString()+"as the checbox is not Enable in the application:");
					Assert.assertTrue(false, "Test case failed as checkbox:"+by.toString()+" is not found:");
				}
			}else{
				System.out.println("unable to select the checkbox :"+by.toString()+" as the checkbox is not visible in the appliation:");
				Assert.assertTrue(false, "Test case failed as checkbox :"+by.toString()+"is not found:");
			}
		}else{
			System.out.println("Unable to select the checkbox :"+by.toString()+" as not found in the application: ");
			Assert.assertTrue(false, "Test Case failed as checkbox :"+by.toString()+"is not found :");	
		}
	}

	public static void unSelectCheckBox(By by){
		WebElement element = waitForElement(by, 2);
		if(element != null){
			if(checkElementvisibility(element, 2)){
				if(checkElementEnable(element, 2)){
					if(element.isSelected()){
						element.click();	
					}
				}else{
					System.out.println("Unable to select the checkbox:"+by.toString()+" as checkbox is not found in the applicaton :");
					Assert.assertTrue(false, "Test case failed as checkbox :"+by.toString()+" is not found :");
				}
			}else{
				System.out.println("Unable to select the checkbox:"+by.toString()+" as the checkbox is not visible in the application :");
				Assert.assertTrue(false, "Test case failed as checkbox :"+by.toString()+" is not found :");
			}
		}else{
			System.out.println("unable to select the checkbox :"+by.toString()+"as not found in the application :");
			Assert.assertTrue(false,"Test case failed as checkbox :"+by.toString()+" is not found :");
		}
	}

	public boolean window_Handle() {

		boolean Iswindow_found=false;
		String window=getDriver().getWindowHandle();
		getDriver().switchTo().window(window);	
		return Iswindow_found;

	}

	public boolean window_Handles() {

		boolean Iswindow_found=false;
		Set<String> allwindows=getDriver().getWindowHandles();
		for(String windows:allwindows) {
			getDriver().switchTo().window(windows);			
		}
		return Iswindow_found;
	}

	public boolean window_HandleExists(String expTitle) {

		boolean Iswindow_found=false;
		String window=getDriver().getWindowHandle();
		getDriver().switchTo().window(window);
		String cur_title=getDriver().getTitle();
		if(expTitle.trim().toLowerCase().contains(cur_title.toLowerCase())) {
			Iswindow_found=true;
		}			
		return Iswindow_found;

	}

	public boolean window_HandlesExists(String expTitle) {

		boolean Iswindow_found=false;
		Set<String> allwindows=getDriver().getWindowHandles();
		for(String windows:allwindows) {
			getDriver().switchTo().window(windows);
			String cur_title=getDriver().getTitle();
			if(expTitle.trim().toLowerCase().contains(cur_title.toLowerCase())) {
				Iswindow_found=true;
			}			
		}
		return Iswindow_found;

	}

	public WebElement waitForElementExists(By by, int timeOut) throws NoSuchElementException{
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
		wait.pollingEvery(Duration.ofMillis(200));
		element = wait.until(ExpectedConditions.presenceOfElementLocated(by));		
		return element;
	}

	public static boolean checkFileExists(String filePath){
		File f = new File(filePath);			
		if (f.exists())
			return true;
		else 
			return false;
	}

	public static boolean checkURL(String valueToCheck){

		boolean isUrlFound=false;		
		try{
			String curUrl =getDriver().getCurrentUrl();
			isUrlFound = curUrl.toLowerCase().contains(valueToCheck);
		}catch(WebDriverException wde){
			System.out.println("Error verifying the url : Exception message is capture :"+ wde.getMessage());
			Assert.assertTrue(false, "unable to verifying the url as WebDriverException is found: ");
		}
		return isUrlFound;		
	}

	public static void selectVisibleText(WebElement WE, String VisibleText){
		Select selObj=new Select(WE);
		selObj.selectByVisibleText(VisibleText);
	}

	//select the dropdown using "select by index", so pass IndexValue as '1','2'.. 
	public static void selectIndexValue(WebElement WE, int IndexValue){
		Select selObj=new Select(WE);
		selObj.selectByIndex(IndexValue);
	}

	//select the dropdown using "select by value", so pass Value as �thirdcolor�
	public static void selectValue(WebElement WE, String Value){
		Select selObj=new Select(WE);
		selObj.selectByValue(Value);
	}


	// FILE UPLOADING USING SENDKEYS 
	public static void uploadFile(WebElement element,String path) {
		//path of the file file like D:\\path\\File.txt
		element.sendKeys(path);
		//click on �Choose file� to upload the desired file
		System.out.println("File is Uploaded Successfully");

	}
	// Check the downloaded file in the download folder
	public boolean isFileDownloaded(String fileDownloadpath, String fileName) {

		boolean flag = false;

		File directory = new File(fileDownloadpath);

		File[] content = directory.listFiles();

		for (int i = 0; i < content.length; i++) {
			if (content[i].getName().equals(fileName))
				return flag=true;
			System.out.println(fileName +": File is downloaded Successfully");
		}
		return flag;
	}


	/*// Download the file and Validate file with Name		
			public void verifywithName(String fileDownloadpath, String fileName,String filetype){

				Assert.assertTrue(isFileDownloaded(fileDownloadpath, fileName+filetype),"Failed to download Expected document");

			}	*/

}
