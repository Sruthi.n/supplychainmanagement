package com.SPC.CommonFunctions;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.SPC.base.ActionEngin;

public class ApplicationCommonFunctions extends ActionEngin {

	public static void login() throws InterruptedException {

		ActionEngin.enterURL(getConfigData().getPropertyValue("url"), "driver");

		/* Login Page Elements */
		WebElement UNAME = ActionEngin.identifyElement("name", getObjRep().getPropertyValue("txt_gos_user_name"));
		WebElement PWD = ActionEngin.identifyElement("name", getObjRep().getPropertyValue("txt_gos_pwd_name"));
		WebElement LOGIN = ActionEngin.identifyElement("id", getObjRep().getPropertyValue("btn_gos_signIN_id"));

		/* Log-In */
		ActionEngin.sendData(UNAME, getExcelData().getSingleData("UserDetails", 2, 1));
		ActionEngin.sendData(PWD, getExcelData().getSingleData("UserDetails", 2, 2));
		LOGIN.click();

		System.out.println("clicked on sign in button successfully");
		Thread.sleep(2000);

	}

	public static void CompaniesNav() throws InterruptedException {

		WebElement Companies_tab = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Companies_tab"));
		Thread.sleep(1000);
		Companies_tab.click();
		Thread.sleep(1000);
		WebElement Companies_submenu = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Companies_submenu"));
		Companies_submenu.click();
		System.out.println("********* Companies Navigation *********");
		Thread.sleep(1000);

	}
	
	public static void ProgramsNav() throws InterruptedException {

		WebElement Configuration_tab = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Configuration_tab"));
		Configuration_tab.click();
		Thread.sleep(1000);
		WebElement Programs_submenu = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Programs_submenu"));
		Programs_submenu.click();
		System.out.println("********* Program Navigation *********");
		Thread.sleep(1000);

	}

		public static void popup_Submit() throws InterruptedException {
		WebElement popup_Submit = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("popup_Submit"));
		popup_Submit.click();
		//ApplicationCommonFunctions.Alert_Accept();
		Thread.sleep(2000);

		/*
		 * try { ScreenShotUtility.screenShot(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
		System.out.println("********* Submit is clicked successfully *********");
		Thread.sleep(2000);
	}

	public static void Submit() throws InterruptedException {
		WebElement Submit_xpath = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("btn_M_AS_Submit_xpath"));
		Submit_xpath.click();
		Thread.sleep(1000);
		/*
		 * try { ScreenShotUtility.screenShot(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
		System.out.println("********* Submit is clicked successfully *********");
		Thread.sleep(2000);
	}

	public static void Cancel() throws InterruptedException {
		WebElement cancel = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("btn_M_AS_cancel_xpath"));
		cancel.click();
		System.out.println("********* Cancel is clicked successfully *********");
		Thread.sleep(2000);

	}
	
	public static void CompanyName_Search(String getCompanyName) throws InterruptedException{
		
		WebElement CompanyName_Search = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CompanyName_Search"));
		ActionEngin.sendData(CompanyName_Search, getCompanyName);
		
		WebElement Search_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Search_btn"));
		Search_btn.click();
		Thread.sleep(2000);
	}
	
	public static void Approval_action() throws InterruptedException{
		try {
			WebElement Approval_action = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Approval_action"));
			Approval_action.click();
			WebElement Alert_ok = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Alert_ok"));
			ActionEngin.waitForElement(Alert_ok, 30);
			Alert_ok.click();
			Thread.sleep(4000);
			
		}catch(org.openqa.selenium.NoSuchElementException ex) {
			System.out.println("Caught No Such Element Exception");
			WebElement ApproveProg_action = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ApproveProg_action"));
			ApproveProg_action.click();
			WebElement Alert_ok = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Alert_ok"));
			ActionEngin.waitForElement(Alert_ok, 30);
			Alert_ok.click();
			Thread.sleep(4000);
		}
		
	}
	
	public static void CreateNewUser_action() throws InterruptedException{

		WebElement CreateNewUser = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CreateNewUser_btn"));
		CreateNewUser.click();
		Thread.sleep(2000);
	}
	
	public static void ViewUser_action() throws InterruptedException{
		WebElement ViewUser_action = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ViewUser_action"));
		ViewUser_action.click();
		Thread.sleep(2000);

		
	}
	
	public static void CreateNewGroup_action() throws InterruptedException{
		
		WebElement ViewGroup_action = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ViewGroup_action"));
		ViewGroup_action.click();
		Thread.sleep(2000);
		WebElement CreateNewGroup = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CreateNewGroup_btn"));
		CreateNewGroup.click();
		Thread.sleep(2000);
	}
	
	public static void AuthorizationMatrix_action() throws InterruptedException{
		
		WebElement ViewAuthorizationMatrix_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ViewAuthorizationMatrix_btn"));
		ViewAuthorizationMatrix_btn.click();
		Thread.sleep(2000);
		WebElement CreateNewRule_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("CreateNewRule_btn"));
		CreateNewRule_btn.click();
		Thread.sleep(2000);
	}
	
	public static void MappingApproval_action() throws InterruptedException{
		
		WebElement MappingApproval_action = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("MappingApproval_action"));
		MappingApproval_action.click();
		Thread.sleep(2000);
		WebElement Approval_action = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ApproveProg_action"));
		Approval_action.click();
		WebElement Alert_ok = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("Alert_ok"));
		Alert_ok.click();
		Thread.sleep(2000);
		
	}
	
	public static void ProgramName_Search(String getProgramName) throws InterruptedException{
		
		WebElement ProgramName_Search = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("ProgramName_Search"));
		ActionEngin.sendData(ProgramName_Search, getProgramName);
		
		WebElement SearchProg_btn = ActionEngin.identifyElement("xpath", getObjRep().getPropertyValue("SearchProg_btn"));
		SearchProg_btn.click();
		Thread.sleep(2000);
	}

	

}
